/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entities.Etudiants;
import entities.Examens;
import entities.Notes;
import entities.Questions;
import entities.Reponses;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import sessions.EtudiantsFacade;
import sessions.ExamensFacade;
import sessions.NotesFacade;
import sessions.QuestionsFacade;
import sessions.ReponsesFacade;

/**
 *
 * @author Joel
 */
@Named(value = "passerExamenManagedBean")
@SessionScoped
public class PasserExamenManagedBean implements Serializable {

    @EJB
    private EtudiantsFacade etudiantsFacade;

    @EJB
    private ExamensFacade examensFacade;
    @EJB
    private ReponsesFacade reponsesFacade;
    @EJB
    private QuestionsFacade questionsFacade;
    @EJB
    private NotesFacade notesFacade;
    

    private int i = 0;

    private int idExam;
    private Examens examen;
    private List<Questions> questions;
    private int nombreQuestions = 0;
    private int noteEtudiant = 0;

    private Questions questionActuelle;
    private List<Reponses> reponsesEtudiant = new ArrayList<>();

    /**
     * Creates a new instance of PasserExamenManagedBean
     */
    public PasserExamenManagedBean() {

    }

    public int getIdExam() {
	return idExam;
    }

    public void setIdExam(int idExam) {
	this.idExam = idExam;
    }

  

    public Examens getExamen() {
	return examen;
    }

    public Questions getQuestionActuelle() {
	return questionActuelle;
    }

    public void load() {
	examen = examensFacade.find(idExam);
	questions = questionsFacade.Question(examen);

	Collections.shuffle(questions);
	nombreQuestions = Math.min(questions.size(), examen.getNombreQuestions());
	i = 0;
	questionActuelle = questions.get(i);

    }




    
    

}
