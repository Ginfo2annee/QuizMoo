/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import entities.Questions;
import entities.Reponses;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import sessions.EtudiantsFacade;
import sessions.QuestionsFacade;
import sessions.ReponsesFacade;

/**
 *
 * @author Hajar MDD
 */
@Named(value = "examenManagedBean")
@SessionScoped
public class ExamenManagedBean implements Serializable {

     private List<Questions> questionlist;
     private List<Reponses> reponseslist;
@EJB
private QuestionsFacade questionfacade ; 
@EJB
    private ReponsesFacade reponsesfacade;
    /**
     * Creates a new instance of ProduitManagedBean
     */
    public ExamenManagedBean() {
    }
    public List<Questions> getQuestions() {
    return questionfacade.findAll()  ; 
    }
    
    public String showDetails(int id){
    
    return "Reponces?idQuestion="+id ; 
    }
    
}
