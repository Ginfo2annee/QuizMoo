package entities;

import entities.Etudiants;
import entities.Examens;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-06-07T11:44:57")
@StaticMetamodel(Notes.class)
public class Notes_ { 

    public static volatile SingularAttribute<Notes, Integer> note;
    public static volatile SingularAttribute<Notes, Examens> idExamen;
    public static volatile SingularAttribute<Notes, Etudiants> matricule;
    public static volatile SingularAttribute<Notes, Integer> idNote;

}