/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kabbaj
 */
@Entity
@Table(name = "examens")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Examens.findAll", query = "SELECT e FROM Examens e")
    , @NamedQuery(name = "Examens.findByIdExamen", query = "SELECT e FROM Examens e WHERE e.idExamen = :idExamen")
    , @NamedQuery(name = "Examens.findByNom", query = "SELECT e FROM Examens e WHERE e.nom = :nom")
    , @NamedQuery(name = "Examens.findByMatiere", query = "SELECT e FROM Examens e WHERE e.matiere = :matiere")
    , @NamedQuery(name = "Examens.findByNombreQuestions", query = "SELECT e FROM Examens e WHERE e.nombreQuestions = :nombreQuestions")})
public class Examens implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdExamen")
    private Integer idExamen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nom")
    private String nom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Matiere")
    private String matiere;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NombreQuestions")
    private int nombreQuestions;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idExamen")
    private List<Notes> notesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idExamen")
    private List<Questions> questionsList;

    public Examens() {
    }

    public Examens(Integer idExamen) {
        this.idExamen = idExamen;
    }

    public Examens(Integer idExamen, String nom, String matiere, int nombreQuestions) {
        this.idExamen = idExamen;
        this.nom = nom;
        this.matiere = matiere;
        this.nombreQuestions = nombreQuestions;
    }

    public Integer getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(Integer idExamen) {
        this.idExamen = idExamen;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public int getNombreQuestions() {
        return nombreQuestions;
    }

    public void setNombreQuestions(int nombreQuestions) {
        this.nombreQuestions = nombreQuestions;
    }

    @XmlTransient
    public List<Notes> getNotesList() {
        return notesList;
    }

    public void setNotesList(List<Notes> notesList) {
        this.notesList = notesList;
    }

    @XmlTransient
    public List<Questions> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<Questions> questionsList) {
        this.questionsList = questionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idExamen != null ? idExamen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Examens)) {
            return false;
        }
        Examens other = (Examens) object;
        if ((this.idExamen == null && other.idExamen != null) || (this.idExamen != null && !this.idExamen.equals(other.idExamen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Examens[ idExamen=" + idExamen + " ]";
    }
    
}
